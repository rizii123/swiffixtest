package com.rizii.swiffix.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.rizii.swiffix.R;
import com.rizii.swiffix.object.SwiffixAnswer;
import com.rizii.swiffix.object.SwiffixQuestion;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rizwansaleem on 17/03/2016.
 */
public class QuestionAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<SwiffixQuestion> mList;
    private static LayoutInflater inflater=null;

    public QuestionAdapter(Context context, ArrayList<SwiffixQuestion> list) {
        this.mContext = context;
        this.mList = list;
        inflater = ( LayoutInflater )mContext.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        SwiffixQuestion object = mList.get(position);
        if(object.getType().equalsIgnoreCase("scroller")) {
            view = inflater.inflate(R.layout.row_scroller, null);
            TextView question = (TextView) view.findViewById(R.id.question);
            question.setText(object.getQuestion());
            // ---
            Spinner spinner = (Spinner) view.findViewById(R.id.spinner);
            final List<String> spinnerList = getSpinnerList(object.getAnswerList());
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>
                    (mContext, android.R.layout.simple_spinner_item, spinnerList);
            // Specify the layout to use when the list of choices appears
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(dataAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Log.d("QuestionAdapter", position + " AND Value is: " + spinnerList.get(position));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } else {
            view = inflater.inflate(R.layout.row_switch, null);
            TextView question = (TextView) view.findViewById(R.id.question);
            question.setText(object.getQuestion());
            // ---
            final Switch mSwitch = (Switch) view.findViewById(R.id.uiswitch);
            mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                }
            });
        }
        return view;
    }

    private List<String>getSpinnerList(ArrayList<SwiffixAnswer> objectList) {
        List<String> answerList = new ArrayList<String>();
        answerList.add("Not Required");
        for(int count = 0; count< objectList.size(); count++) {
            answerList.add(objectList.get(count).getAnswer());
        }
        return answerList;
    }
}
