package com.rizii.swiffix.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.rizii.swiffix.R;
import com.rizii.swiffix.object.SwiffixObject;

import java.util.ArrayList;

/**
 * Created by rizwansaleem on 17/03/2016.
 */
public class ListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<SwiffixObject> mList;
    private static LayoutInflater inflater=null;

    public ListAdapter(Context context, ArrayList<SwiffixObject> list) {
        this.mContext = context;
        this.mList = list;
        inflater = ( LayoutInflater )mContext.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = inflater.inflate(R.layout.row_main, null);
        TextView name = (TextView) vi.findViewById(R.id.item_name);
        name.setText(mList.get(position).getName());
        return vi;
    }
}
