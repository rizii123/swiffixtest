package com.rizii.swiffix.object;

import java.util.ArrayList;

/**
 * Created by rizwansaleem on 17/03/2016.
 */
public class SwiffixQuestion {

    private int id;
    private String question;
    private String type;
    private int level;
    private boolean isActive;
    private ArrayList<SwiffixAnswer> answerList;

    public SwiffixQuestion() {

    }

    public SwiffixQuestion(int id, String question, String type, int level, boolean isActive, ArrayList<SwiffixAnswer> answerList) {
        this.id = id;
        this.question = question;
        this.type = type;
        this.level = level;
        this.isActive = isActive;
        this.answerList = answerList;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public ArrayList<SwiffixAnswer> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(ArrayList<SwiffixAnswer> answerList) {
        this.answerList = answerList;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
}
