package com.rizii.swiffix.object;

import java.util.ArrayList;

/**
 * Created by rizwansaleem on 17/03/2016.
 */
public class SwiffixObject {

    private int id;
    private String name;
    private double hourlyPrice;
    private double minCost;
    private double timeInterval;
    private ArrayList<SwiffixService> serviceArray;

    public ArrayList<SwiffixService> getServiceArray() {
        return serviceArray;
    }

    public void setServiceArray(ArrayList<SwiffixService> serviceArray) {
        this.serviceArray = serviceArray;
    }

    public SwiffixObject() {

    }

    public SwiffixObject(int id, String name, double hourlyPrice, double minCost, double timeInterval, ArrayList<SwiffixService> serviceArray) {
        this.id = id;
        this.name = name;
        this.hourlyPrice = hourlyPrice;
        this.minCost = minCost;
        this.timeInterval = timeInterval;
        this.serviceArray = serviceArray;
    }

    public double getHourlyPrice() {
        return hourlyPrice;
    }

    public void setHourlyPrice(double hourlyPrice) {
        this.hourlyPrice = hourlyPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMinCost() {
        return minCost;
    }

    public void setMinCost(double minCost) {
        this.minCost = minCost;
    }

    public double getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(double timeInterval) {
        this.timeInterval = timeInterval;
    }
}
