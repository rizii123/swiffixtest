package com.rizii.swiffix.object;

/**
 * Created by rizwansaleem on 17/03/2016.
 */
public class SwiffixAnswer {

    private int id;
    private String answer;
    private double hours;
    private int level;
    private boolean isActive;

    public SwiffixAnswer() {

    }

    public SwiffixAnswer(int id, String answer, double hours, int level, boolean isActive) {
        this.id = id;
        this.answer = answer;
        this.hours = hours;
        this.level = level;
        this.isActive = isActive;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
}
