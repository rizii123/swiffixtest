package com.rizii.swiffix.object;

/**
 * Created by rizwansaleem on 17/03/2016.
 */
public class SwiffixService {

    private int id;
    private int specializationId;
    private String name;
    private String imageUrl;
    private int level;
    private boolean isActive;

    public SwiffixService() {

    }

    public SwiffixService(int id, int specializationId, String name, String imageUrl, int level, boolean isActive) {

        this.id = id;
        this.specializationId = specializationId;
        this.name = name;
        this.imageUrl = imageUrl;
        this.level = level;
        this.isActive = isActive;
    }

    public int getSpecializationId() {
        return specializationId;
    }

    public void setSpecializationId(int specializationId) {
        this.specializationId = specializationId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
}
