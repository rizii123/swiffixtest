package com.rizii.swiffix.application;

import android.app.Application;

import com.rizii.swiffix.utility.NukeSSLCerts;

/**
 * Created by rizwansaleem on 17/03/2016.
 */
public class SwiffixApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // Nuke SSL Cert
        NukeSSLCerts.nuke();
    }
}
