package com.rizii.swiffix.utility;

import com.rizii.swiffix.object.SwiffixObject;

import java.util.ArrayList;

/**
 * Created by rizwansaleem on 17/03/2016.
 */
public class CONSTANTS {

    public static final String LOAD_SERVICES = "https://www.swiffix.com/api/specializations?expand=services";
    public static final String QUESTIONS_URL = "https://swiffix.com/api/questions/";
    public static ArrayList<SwiffixObject> mGlobalList;
}
