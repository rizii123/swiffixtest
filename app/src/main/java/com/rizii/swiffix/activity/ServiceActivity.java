package com.rizii.swiffix.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.rizii.swiffix.R;
import com.rizii.swiffix.adapter.ServiceListAdapter;
import com.rizii.swiffix.object.SwiffixService;
import com.rizii.swiffix.utility.CONSTANTS;

import java.util.ArrayList;

public class ServiceActivity extends AppCompatActivity {

    private ListView mServiceList;
    private ServiceListAdapter mAdapter;
    ArrayList<SwiffixService> mSwiffixList;
    int currentPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicelist);
        Intent intent = getIntent();
        currentPosition = intent.getIntExtra("position", 0);
        // ---
        mSwiffixList = CONSTANTS.mGlobalList.get(currentPosition).getServiceArray();
        // --
        mServiceList = (ListView) findViewById(R.id.service_list);
        mAdapter = new ServiceListAdapter(getApplicationContext(), CONSTANTS.mGlobalList.get(currentPosition).getServiceArray());
        mServiceList.setAdapter(mAdapter);

        mServiceList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), QuestionActivity.class);
                intent.putExtra("object", currentPosition);
                intent.putExtra("service", mSwiffixList.get(position).getId());
                startActivity(intent);
            }
        });
    }
}
