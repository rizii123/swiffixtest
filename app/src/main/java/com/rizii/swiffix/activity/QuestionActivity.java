package com.rizii.swiffix.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.rizii.swiffix.R;
import com.rizii.swiffix.adapter.QuestionAdapter;
import com.rizii.swiffix.object.SwiffixAnswer;
import com.rizii.swiffix.object.SwiffixQuestion;
import com.rizii.swiffix.utility.CONSTANTS;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class QuestionActivity extends AppCompatActivity {

    private ListView mQuestionList;
    private ArrayList<SwiffixQuestion> swiffixArray;
    private QuestionAdapter mAdapter;
    int serviceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        swiffixArray = new ArrayList<>();
        mQuestionList = (ListView) findViewById(R.id.question_list);
        Intent intent = getIntent();
        serviceId = intent.getIntExtra("service", 0);
        // ---
        loadQuestionData();
    }

    private void loadQuestionData() {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(CONSTANTS.QUESTIONS_URL + serviceId,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.d("QuestionActivity", "Response" + response.toString());
                            for (int count = 0; count < response.length(); count++) {
                                JSONObject object = response.getJSONObject(count);
                                SwiffixQuestion obj = createQuestionObject(object);
                                swiffixArray.add(obj);
                            }
                            createList();
                        } catch (Exception e) {

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        // Making a network call
        Volley.newRequestQueue(this).add(jsonArrayRequest);
    }


    private SwiffixQuestion createQuestionObject(JSONObject object) {
        int id = object.optInt("id", 0);
        String question = object.optString("question", "");
        String type = object.optString("type_ui", "");
        int level = object.optInt("level");
        boolean isActive = object.optBoolean("active");
        ArrayList<SwiffixAnswer> list = getAnswerList(object.optJSONArray("answers"));
        SwiffixQuestion questionObject = new SwiffixQuestion(id, question, type, level, isActive, list);
        return questionObject;
    }


    private ArrayList<SwiffixAnswer> getAnswerList(JSONArray arrayObject) {
        ArrayList<SwiffixAnswer> array = new ArrayList<>();
        for(int count = 0; count < arrayObject.length(); count++) {
            JSONObject object = arrayObject.optJSONObject(count);
            int id = object.optInt("id", 0);
            String answer = object.optString("answer", "");
            double hours = object.optDouble("hours", 0);
            int level = object.optInt("level");
            boolean active = object.optBoolean("active");
            // ---
            SwiffixAnswer answerObject = new SwiffixAnswer(id, answer, hours, level, active);
            array.add(answerObject);
        }
        return array;
    }

    private void createList() {
        mAdapter = new QuestionAdapter(getApplicationContext(), swiffixArray);
        this.mQuestionList.setAdapter(mAdapter);
    }
}
