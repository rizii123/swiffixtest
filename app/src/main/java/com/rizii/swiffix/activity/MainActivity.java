package com.rizii.swiffix.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.rizii.swiffix.R;
import com.rizii.swiffix.adapter.ListAdapter;
import com.rizii.swiffix.object.SwiffixObject;
import com.rizii.swiffix.object.SwiffixService;
import com.rizii.swiffix.utility.CONSTANTS;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView mListView;
    private ListAdapter mListAdapter;
    private ArrayList<SwiffixObject> swiffixArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // --
        swiffixArray = new ArrayList<SwiffixObject>();
        // Initialize UI
        initializeUI();
        loadSwiffixData();
    }

    private void initializeUI() {
        mListView = (ListView) findViewById(R.id.main_list);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("mainActivity", "Position is: " + position);
                Intent intent = new Intent(getApplicationContext(), ServiceActivity.class);
                intent.putExtra("position", position);
                startActivity(intent);
            }
        });
    }

    /*
     * Loading Swiffix Array from network
     */
    private void loadSwiffixData() {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(CONSTANTS.LOAD_SERVICES,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int count = 0; count < response.length(); count++) {
                                JSONObject object = response.getJSONObject(count);
                                SwiffixObject obj = createSwiffixObject(object);
                                swiffixArray.add(obj);
                            }
                            CONSTANTS.mGlobalList = swiffixArray;
                            createList();
                        } catch (JSONException e) {

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
                public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        // Making a network call
        Volley.newRequestQueue(this).add(jsonArrayRequest);
    }

    /*
     * Creating a Swiffix Array of Objects including services
     */
    private SwiffixObject createSwiffixObject(JSONObject object) {
        int id = object.optInt("id", 0);
        String name = object.optString("name", "");
        double hourlyPrice = object.optDouble("hourly_price");
        double minCost = object.optDouble("min_cost");
        double timeInterval = object.optDouble("time_interval");
        ArrayList<SwiffixService> list = getSwiffixServiceList(object.optJSONArray("services"));
        // ---
        SwiffixObject mObject = new SwiffixObject(id, name, hourlyPrice, minCost, timeInterval, list);
        return mObject;
    }

    private ArrayList<SwiffixService> getSwiffixServiceList(JSONArray arrayObject) {
        ArrayList<SwiffixService> array = new ArrayList<>();
        for(int count = 0; count < arrayObject.length(); count++) {
            JSONObject object = arrayObject.optJSONObject(count);
            int id = object.optInt("id", 0);
            int specialId = object.optInt("specialization_id", 0);
            String name = object.optString("name", "");
            String imageUrl = object.optString("image", "");
            int level = object.optInt("level");
            boolean active = object.optBoolean("active");
            // ---
            SwiffixService service = new SwiffixService(id, specialId, name, imageUrl, level, active);
            array.add(service);
        }
        return array;
    }

    private void createList() {
        mListAdapter = new ListAdapter(getApplicationContext(), swiffixArray);
        this.mListView.setAdapter(mListAdapter);
    }
}
